/**
 * Classe que ens permet fer el seguiment d'un alumne en concret.
 *
 * @author Pol Agustina Prats
 * @see Problemes
 */
class Alumne {

    val problemesAlumne = Problemes()

    /**
     * Funció que ens permet seguir amb l'itinerari de l'alumne.
     */
    fun seguirItinerari(){
        val problemes = this.problemesAlumne.resolts(true)
        var i = 0
        var seguir = true
        do{
            problemes[i].imprimirProblema(i+1)
            println("Vols resoldre'l?")
            println("     1. Si     2. No     3. Tornar al menú")
            do{
                print("Escull una opció vàlida:   ")
                var respostaUsuari = escaner.next()
                when(respostaUsuari){
                    "1" -> {
                        println()
                        problemes[i].resoldreProblema()
                        i++
                    }
                    "2" -> {
                        println()
                        i++
                    }
                    "3" -> {
                        seguir=false
                    }
                    else -> respostaUsuari = "error"
                }
            }while (respostaUsuari=="error")
        }while (i in problemes.indices&&seguir)
    }

    /**
     * Funció que mostra els problemes resolts per l'alumne.
     */
    fun historicProblemes(){
        val problemes = this.problemesAlumne.resolts(false)
        for (problema in problemes) problema.imprimirEstat()
        menuAlumne()
    }
}