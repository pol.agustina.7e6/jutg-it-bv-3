# JutgITB
#### Versió: 2

### Descripció:

JutgITB és el software més complert i fet servir arreu per resoldre i corregir problemes. Gràcies a la seva simplicitat
única l'alumne pot gaudir d'una experiència educativa sense precedents.
El sistema de resolució de problemes és molt senzill. Per cada problema plantejat l'alumne pot decidir si resoldre'l o no.
En alguns casos es passarà al problema següent i en alguns d'altres l'alumne tindra accés de nou al menú per decidir què
vol treballar. Al final de cada problema resolt es mostra el recull dels intents que l'alumne ha emprat per arribar a la
solució.
En general JutgITB és una aplicació molt intuitiva, encarada precisament a donar una experiència còmode tant a alumnes com a
professors.

### Instal·lació i execució

1. Descarrega't el projecte **JutgITBv2** des de *Git Lab*
2. Obre el projecte amb el programa *Intellij Idea*
3. Des del navegador del projecte obre el fitxer **main.kt** del projecte situat 
a l'adreça "JutgITBv2/src/main/kotlin/main.kt"
4. Executa la funció **main**

### Llicència:

MIT License

Copyright (c) [2023] [Pol Agustina Prats]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
